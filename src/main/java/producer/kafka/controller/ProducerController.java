package producer.kafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import producer.kafka.sender.Sender;

@RestController
@RequestMapping("/producer")
public class ProducerController {

	@Value("${kafka.topic.helloworld}")
	private String helloWorldTopic;

	@Autowired
	private Sender sender;

	@RequestMapping(value = "/send", method = RequestMethod.POST)
	public String sendMessage(@RequestBody String message) {
		sender.send(helloWorldTopic, message);
		return "success";
	}
}