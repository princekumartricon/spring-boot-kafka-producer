Before starting kafka server, add the following lines in kafka_2.12-1.0.0/config/server.properties file to provide access for message push to kafka topic from outside of linux machine:-
----------------------------------------------------------------------------------------------------------

listeners=PLAINTEXT://:9092
port=9092

advertised.listeners=PLAINTEXT://172.16.18.111:9092

Start zookeeper using following command:-
------------------------------------------
bin/zookeeper-server-start.sh config/zookeeper.properties

Start kafka server using following command:-
---------------------------------------------
bin/kafka-server-start.sh config/server.properties

Run the following commands to build and start producer:-
---------------------------------------------------------
export PATH=$PATH:/home/medium/softwares/apache-maven-3.5.2/bin

mvn clean install package
mvn spring-boot:run


The producer will start on 8080 port


Hit the following url to send message to kafka topic:-
-------------------------------------------------------
Url:	http://172.16.18.111:8080/producer/send
Method:	POST
Sample Payload:-
{
	"name":"a"
}